import io from 'socket.io-client'
import url from 'url'

import tileStore from '../stores/tileStore'
import playerStore from '../stores/playerStore'
import viewStore from '../stores/viewStore'
import settingStore from '../stores/settingStore'
import * as chatUi from '../input/ui/chat'
import * as sharedConsts from '@kaku/common/consts'


let _socket = null


function connect(initData) {
  return new Promise((resolve) => {
    _socket = io(process.env.KAKU_FRONTEND_OVERWORLD_URL)

    _socket.emit(
      'init',
      {
        viewSize: initData.viewSize
      },
      (response) => {
        playerStore.initLocal({
          id: response.id,
          color: '#000000'
        })

        for (const remotePlayer of response.players) {
          playerStore.addRemote(remotePlayer)
        }

        _socket.on('player-connected', _onPlayerConnected)
        _socket.on('player-disconnected', _onPlayerDisconnected)
        _socket.on('sync', _onSync)
        _socket.on('sync-tiles', _onSyncTiles)
        _socket.on('chat', _onChat)
        _socket.on('paint', _onPaint)
        _socket.on('paint-done', _onPaintDone)

        window.setInterval(_sendSync, sharedConsts.LOCAL_SYNC_TIME)
        resolve()
      }
    )
  })
}

function _onPlayerConnected(data) {
  playerStore.addRemote(data)
}

function _onPlayerDisconnected(data) {
  playerStore.removeRemote(data.id)
}

function _onSync(data) {
  for (let player of data.players) {
    playerStore.syncRemote(player)
  }
}

function _onSyncTiles(data) {
  _syncTiles(data.positions)
}

function _syncTiles(positions) {
  for (const position of positions) {
    tileStore.addLoader(position)
  }

  const groupedPositions = []
  const groupCount = Math.ceil(
    positions.length / sharedConsts.TILE_SERVER_MAX_COUNT)

  for (let i = 0; i < groupCount; i++) {
    groupedPositions.push(positions.slice(
      i * sharedConsts.TILE_SERVER_MAX_COUNT,
      i * sharedConsts.TILE_SERVER_MAX_COUNT +
        sharedConsts.TILE_SERVER_MAX_COUNT))
  }

  const reqPromises = []
  for (const positions of groupedPositions) {
    reqPromises.push(new Promise((resolve) => {
      const tileUrlQuery = url.format({
        query: {
          pos: positions.map(JSON.stringify)
        }
      })

      const tileUrl = url.resolve(process.env.KAKU_FRONTEND_TILE_BASE,
        tileUrlQuery)

      window.fetch(tileUrl)
        .then((res) => res.blob())
        .then((blob) => {
          const blobUrl = window.URL.createObjectURL(blob)
          const img = new Image()

          img.src = blobUrl
          img.addEventListener('load', () => {
            for (let i = 0; i < positions.length; i++) {
              tileStore.tileSet.initTile(positions[i], img, i)
              tileStore.completeLoader(positions[i])
            }

            window.URL.revokeObjectURL(blobUrl)
            resolve()
          })
        })
      })
    )
  }

  return new Promise((resolve) => {
    Promise.all(reqPromises)
      .then(() => resolve(positions))
  })
}

function _onChat(data) {
  chatUi.addLine(data.name, data.message)
}

function _onPaint(data) {
  if (!settingStore.debug.disableLocalPaints)
    tileStore.addLine(data)
}

function _onPaintDone(data) {
  _syncTiles(data.positions)
    .then(() => {
      if (!settingStore.debug.disableLocalPaints)
        data.ids.map((id) => tileStore.removeLine(id))
    })
}

function _sendSync() {
  _socket.emit('sync', {
    position: playerStore.local.position,
    painting: playerStore.local.painting,
    line: playerStore.local.line,
    size: playerStore.local.size,
    color: playerStore.local.color,

    pan: viewStore.pan,
    scale: viewStore.scale
  })
}

function sendPaint(line, color, tempId) {
  _socket.emit('paint', {
    line,
    color
  }, (res) => {
    if (!settingStore.debug.disableLocalPaints)
      tileStore.syncLocalLineId(tempId, res.id)
  })
}

function sendSyncResize() {
  _socket.emit('sync-resize', {
    viewSize: viewStore.size
  })
}

function sendSyncName() {
  _socket.emit('sync-name', {
    name: playerStore.local.name
  })
}

function sendChatMessage(message) {
  _socket.emit('chat', {
    message
  })
}

function sendJoin() {
  _socket.emit('join', {
    name: playerStore.local.name
  })
}

export default {
  connect,
  sendPaint,
  sendSyncResize,
  sendSyncName,
  sendChatMessage,
  sendJoin
}
