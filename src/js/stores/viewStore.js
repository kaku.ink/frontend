import * as _ from 'lodash'
import * as consts from '../consts'


class _ViewStore {
  constructor() {
    this.size = [0, 0]
    this.pan = [0, 0]
    this.scale = 1
    this.tool = consts.TOOL_PAN

    this.spectating = true
  }

  setSize(size) {
    this.size = size
  }

  setPan(pan) {
    this.pan = pan
  }

  setScale(scale) {
    this.scale = _.clamp(scale, 0.33, 2)
  }
}


export default new _ViewStore()
