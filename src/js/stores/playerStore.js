import dt from '../utils/deltaTime'
import * as sharedConsts from '@kaku/common/consts'


class _PlayerState {
  constructor(initData = { id: null, color: '#000000', name: null }) {
    this.id = initData.id

    this.name = initData.name
    this.color = initData.color

    this.position = [0, 0]
    this.size = 8

    this.line = []
    this.painting = false

    this._interp = {
      value: 1,
      target: this.position,
      origin: this.position,
      queued: []
    }
  }
}


class _PlayerStateStore {
  constructor() {
    this.local = null
    this.remotes = []
  }

  initLocal(data) {
    this.local = new _PlayerState(data)
  }

  setLocalPosition(position) {
    this.local.position = position
  }

  addRemote(data) {
    if (data.id === this.local.id)
      return

    this.remotes.push(new _PlayerState(data))
  }

  removeRemote(id) {
    this.remotes = this.remotes.filter((player) => { return player.id !== id })
  }

  syncRemote(data) {
    if (data.id === this.local.id)
      return

    let player = this.remotes.find((player) => { return player.id === data.id })
    player._interp.queued.push(data.position)
    player.painting = data.painting
    player.line = data.line
    player.color = data.color
    player.size = data.size
    player.name = data.name
  }

  cullRemoteInterp() {
    for (const player of this.remotes) {
      if (player._interp.queued.length >= 1) {
        player._interp.value = 1
        player._interp.target = player._interp.queued.pop()
        player._interp.origin = player._interp.target
        player.position = player._interp.target
        player._interp.queued = []
      }
    }
  }

  tickRemoteInterp() {
    function _updatePosition(player) {
      if (player._interp.value >= 1) {
        player.position = player._interp.target
      } else if (player._interp.value <= 0) {
        player.position = player._interp.origin
      } else {
        let diff = [
          player._interp.target[0] - player._interp.origin[0],
          player._interp.target[1] - player._interp.origin[1]
        ]

        player.position = [
          player._interp.origin[0] + (diff[0] * player._interp.value),
          player._interp.origin[1] + (diff[1] * player._interp.value)
        ]
      }
    }

    function _nextQueued(player) {
      player.position = player._interp.target
      player._interp.target = player._interp.queued.shift()
      player._interp.origin = player.position
    }

    for (let player of this.remotes) {
      if (player._interp.value < 1) {
        player._interp.value += dt.delta / sharedConsts.REMOTE_SYNC_TIME * 1.2
      }

      while (player._interp.value >= 1 && player._interp.queued.length >= 1) {
        _nextQueued(player)
        player._interp.value -= 1
      }

      _updatePosition(player)
    }
  }
}


export default new _PlayerStateStore()
