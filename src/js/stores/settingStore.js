import * as _ from 'lodash'


class _SettingStore {
  constructor() {
    this.chatNotifications = this. _get('chatNotifications', true)
    this.debug = {
      showTileOverlay: this._get('debug.showTileOverlay', false),
      disableLocalPaints: this._get('debug.disableLocalPaints', false)
    }
  }

  set(item, value) {
    window.localStorage.setItem(item, value)
    _.set(this, item, value)
  }



  _get(item, def) {
    return window.localStorage.getItem(item) !== null ?
      (window.localStorage.getItem(item) === 'true' ? true : false) : def
  }
}


export default new _SettingStore()
