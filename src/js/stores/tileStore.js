import { TileSet } from '@kaku/common/tiles'
import { Map2D } from '@kaku/common/utils'


function _canvasCtxCreator(width, height) {
  const ctx = document.createElement('canvas').getContext('2d')
  ctx.canvas.width = width
  ctx.canvas.height = height

  return ctx
}


class _TileStore {
  constructor() {
    this.tileSet = new TileSet(_canvasCtxCreator)
    this.lines = []
    this.loaders = new Map2D()

    this._localLineTempId = 0
  }

  addLoader(position) {
    this.loaders.set(position, {
      rotation: Math.random() * Math.PI * 2,
      fading: false,
      fade: 1
    })
  }

  completeLoader(position) {
    this.loaders.get(position).fading = true
  }

  removeLoader(position) {
    this.loaders.delete(position)
  }

  addLine(line) {
    this.lines.push(line)
  }

  addLocalLine(line) {
    this.lines.push({
      id: -(++this._localLineTempId),
      line: line.line,
      color: line.color
    })

    return -this._localLineTempId
  }

  syncLocalLineId(tempId, realId) {
    const line = this.lines.find((l) => l.id === tempId)
    if (line)
      line.id = realId
  }

  removeLine(id) {
    const lineIndex = this.lines.findIndex((l) => l.id === id )
    if (lineIndex >= 0)
      this.lines.splice(lineIndex, 1)
  }
}


export default new _TileStore()
