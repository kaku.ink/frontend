import 'babel-polyfill';
import 'whatwg-fetch';

import net from './net'
import playerStore from './stores/playerStore'
import viewStore from './stores/viewStore'
import dt from './utils/deltaTime'
import * as input from './input'
import { Renderer } from './rendering'

import '../scss/index.scss'


window.localStorage.debug = 'game:*'


class App {
  constructor() {
    this.canvas = window.document.getElementById('canvas')
    this.canvas.width = window.document.body.clientWidth;
    this.canvas.height = window.document.body.clientHeight;

    viewStore.setSize([this.canvas.width, this.canvas.height])

    this.renderer = new Renderer(this.canvas)
  }

  start() {
    net.connect({
      name: window.localStorage.getItem('name'),
      viewSize: [this.canvas.width, this.canvas.height]
    }).then(() => {
      input.attach(this.canvas)
      this.loop()
    })
  }

  loop() {
    dt.tick()

    playerStore.tickRemoteInterp()
    this.renderer.render()

    window.requestAnimationFrame(() => this.loop())
  }
}


window.document.addEventListener('DOMContentLoaded', () => {
  const _app = new App()
  _app.start()
});
