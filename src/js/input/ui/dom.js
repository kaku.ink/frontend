import playerStore from '../../stores/playerStore'
import viewStore from '../../stores/viewStore'
import net from '../../net'


function onFullscreen() {
  if (!window.document.webkitFullscreenElement) {
    window.document.documentElement.webkitRequestFullscreen()
    window.document.getElementById('fullscreen')
      .classList
      .add('is-selected')
  } else {
    window.document.webkitExitFullscreen()
    window.document.getElementById('fullscreen')
      .classList
      .remove('is-selected')
  }
}

function onVisibilityChange() {
  if (window.document.hidden)
    playerStore.cullRemoteInterp()
}

function onResize() {
  const canvas = window.document.getElementById('canvas')
  canvas.width = window.document.body.clientWidth;
  canvas.height = window.document.body.clientHeight;

  viewStore.setSize([canvas.width, canvas.height])
  net.sendSyncResize()
}


export function attach() {
  window.document.addEventListener('visibilitychange', onVisibilityChange)
  window.document.getElementById('fullscreen').addEventListener(
    'click', onFullscreen)

  window.document.addEventListener('dragstart', (e) => {
    e.preventDefault()
    return false
  })

  window.addEventListener('resize', onResize)
}
