export function isVisible(dom) {
  const style = window.getComputedStyle(dom)
  return style.getPropertyValue('visibility') === 'visible'
}


export function toggleVisible(dom) {
  const visible = isVisible(dom)

  if (visible) {
    dom.classList.add('is-hidden')
    dom.classList.remove('is-visible')
  } else {
    dom.classList.add('is-visible')
    dom.classList.remove('is-hidden')
  }

  return !visible
}
