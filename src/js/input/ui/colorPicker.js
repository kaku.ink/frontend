import tinycolor from 'tinycolor2'
import * as util from './util'
import playerStore from '../../stores/playerStore'

const COLORS = 12
const SHADES = 5
const SHADE_EDGE = 0.05

const COLOR_TO_BUTTONS = {}


function onColorClick(e, color) {
  COLOR_TO_BUTTONS[playerStore.local.color].classList.remove('is-selected')
  COLOR_TO_BUTTONS[color].classList.add('is-selected')

  playerStore.local.color = color
  window.document
    .getElementById('color-picker-current')
    .style
    .backgroundColor = tinycolor(color).toHexString()

  onColorPickerClick()
}

function onColorPickerClick() {
  util.toggleVisible(window.document.getElementById('color-picker'))

  window.document
    .getElementById('color-picker-button')
    .classList.toggle('is-selected')
}


function _createColorPicker() {
  function __createGroup(h, hslFunc) {
    const group = window.document.createElement('div')
    group.classList.add('color-picker-group')

    for (let l = 1; l <= SHADES; l++) {
      const color = tinycolor(hslFunc(h, l)).toHexString()

      const button = window.document.createElement('button')
      button.addEventListener('click', (e) => { onColorClick(e, color) })
      group.appendChild(button)

      const graphic = window.document.createElement('div')
      graphic.classList.add('color-picker-graphic')
      graphic.style.backgroundColor = color
      button.appendChild(graphic)

      COLOR_TO_BUTTONS[color] = button
    }

    return group
  }

  const colorPicker = window.document.getElementById('color-picker')
  colorPicker.appendChild(
    __createGroup(0, (h, l) => ({
      h: 0,
      s: 0,
      l: (1 / (SHADES - 1)) * (l - 1)
    })
  ))

  for (let h = 0; h < COLORS; h++) {
    colorPicker.appendChild(
      __createGroup(h, (h, l) => ({
        h: (h / COLORS) * 360,
        s: 1,
        l: SHADE_EDGE + (((1 - SHADE_EDGE * 2) / SHADES) * l)
      })
    ))
  }
}


export function attach() {
  _createColorPicker()

  window.document
    .getElementById('color-picker-current')
    .style
    .backgroundColor = tinycolor(playerStore.local.color).toHexString()

  window.document
    .getElementById('color-picker-button')
    .addEventListener('click', onColorPickerClick)
}
