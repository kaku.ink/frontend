import * as util from './util'
import playerStore from '../../stores/playerStore'
import settingStore from '../../stores/settingStore'
import net from '../../net'
import * as consts from '../../consts'

const ENTER_KEY = 13
const ESC_KEY = 27


function onEnterPress(e) {
  if (e.keyCode === ENTER_KEY) {
    _send()
  } else if (e.keyCode === ESC_KEY) {
    _toggleVisible()
  }
}


function _send() {
  let message = window.document.getElementById('chat-input').value.trim()
  message.substring(0, 256)

  if (message != "") {
    addLine(playerStore.local.name, message)
    net.sendChatMessage(message)
  }

  window.document.getElementById('chat-input').value = ''
}

function _toggleVisible() {
  const visible = util.toggleVisible(window.document.getElementById('chat'))
  window.document.getElementById('chat-open').classList.toggle('is-selected')

  if (visible) {
    window.document.getElementById('chat-open').classList
      .remove('has-notification')
  }
}

function _focus() {
  window.document.getElementById('chat-input').focus()
}


export function addLine(name, message) {
  const lineDom = window.document.createElement('div')
  lineDom.className = 'chat-line'

  const nameDom = window.document.createElement('div')
  nameDom.className = 'chat-name'
  nameDom.textContent = `${name}:`
  lineDom.appendChild(nameDom)

  const textDom = window.document.createElement('div')
  textDom.className = 'chat-text'
  textDom.textContent = message
  lineDom.appendChild(textDom)

  const chatBoxDom = window.document.getElementById('chat-box')
  chatBoxDom.appendChild(lineDom)
  chatBoxDom.scrollTop = chatBoxDom.scrollHeight

  if (chatBoxDom.children.length > consts.CHAT_MAX_LINES)
    chatBoxDom.removeChild(chatBoxDom.children.item(0))

  if (settingStore.chatNotifications) {
    if (!util.isVisible(window.document.getElementById('chat')))
      window.document.getElementById('chat-open').classList
        .add('has-notification')
  }
}


export function attach() {
  window.document.getElementById('chat-input')
    .addEventListener('keydown', onEnterPress)

  window.document.getElementById('chat-send')
    .addEventListener('click', _send)

  window.document.getElementById('chat-open')
    .addEventListener('click', _toggleVisible)

  window.document.getElementById('chat-box')
    .addEventListener('click', _focus)

  window.document.getElementById('chat-close')
    .addEventListener('click', _toggleVisible)
}
