import * as dom from './dom'
import * as colorPicker from './colorPicker'
import * as tools from './tools'
import * as modals from './modals'
import * as chat from './chat'
import * as splash from './splash'


export function attach() {
  dom.attach()
  colorPicker.attach()
  tools.attach()
  modals.attach()
  chat.attach()
  splash.attach()
}
