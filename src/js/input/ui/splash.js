import playerStore from '../../stores/playerStore'
import viewStore from '../../stores/viewStore'
import net from '../../net'
import * as consts from '../../consts'


function onJoinClick() {
  window.localStorage.setItem('name',
    window.document.getElementById('splash-name').value)

  playerStore.local.name = window.document.getElementById('splash-name').value
  net.sendJoin()
  viewStore.tool = consts.TOOL_PEN

  window.document.getElementById('splash').classList.add('is-hidden')
}


export function attach() {
  window.document.getElementById('splash-join')
    .addEventListener('click', onJoinClick)

  window.document.getElementById('splash-name').value =
    window.localStorage.getItem('name')
}
