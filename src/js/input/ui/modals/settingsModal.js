import playerStore from '../../../stores/playerStore'
import settingStore from '../../../stores/settingStore'
import net from '../../../net'


function onHelpClick() {
  window.document.getElementById('settings-modal')
    .classList.toggle('is-visible')
}

function onCloseClick() {
  window.document.getElementById('settings-modal')
    .classList.toggle('is-visible')
}

function onCanvasClick() {
  window.document.getElementById('settings-modal')
    .classList.remove('is-visible')
}


function onNameUnfocus(e) {
  playerStore.local.name = e.currentTarget.value
  window.localStorage.set('name', playerStore.local.name)
  net.sendSyncName()
}

function onShowTileOverlayChange(e) {
  settingStore.set('debug.showTileOverlay', e.currentTarget.checked)
}

function onDisableLocalPaints(e) {
  settingStore.set('debug.disableLocalPaints', e.currentTarget.checked)
}

function onChatNotifications(e) {
  settingStore.set('chatNotifications', e.currentTarget.checked)
  window.document.getElementById('chat-open').classList
    .remove('has-notification')
}


function _hookCheckbox(id, func, setting) {
  window.document.getElementById(id)
    .addEventListener('change', func)

  if (setting)
    window.document.getElementById(id).setAttribute('checked', 'checked')
  else
    window.document.getElementById(id).removeAttribute('checked')
}


export function attach() {
  window.document.getElementById('settings-button')
    .addEventListener('click', onHelpClick)

  window.document.getElementById('settings-modal')
    .querySelector('button.modal-close')
    .addEventListener('click', onCloseClick)

  window.document.getElementById('canvas')
    .addEventListener('mousedown', onCanvasClick)

  window.document.getElementById('canvas')
    .addEventListener('touchstart', onCanvasClick)

  window.document.getElementById('settings-name').value =
    window.localStorage.getItem('name')
  window.document.getElementById('settings-name')
    .addEventListener('blur', onNameUnfocus)

  _hookCheckbox(
    'settings-show-tile-overlay',
    onShowTileOverlayChange,
    settingStore.debug.showTileOverlay)

  _hookCheckbox(
    'settings-disable-local-paints',
    onDisableLocalPaints,
    settingStore.debug.disableLocalPaints)

  _hookCheckbox(
    'settings-chat-notifications',
    onChatNotifications,
    settingStore.chatNotifications)

  Array.from(window.document.querySelectorAll('.modal-container form'))
    .map((form) => {
      form.addEventListener('submit', (e) => e.preventDefault())
    })
}
