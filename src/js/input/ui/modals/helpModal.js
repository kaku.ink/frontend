function onHelpClick() {
  window.document.getElementById('help-modal').classList.toggle('is-visible')
}

function onCloseClick() {
  window.document.getElementById('help-modal').classList.toggle('is-visible')
}

function onCanvasClick() {
  window.document.getElementById('help-modal').classList.remove('is-visible')
}

export function attach() {
  window.document.getElementById('help-button')
    .addEventListener('click', onHelpClick)

  window.document.getElementById('help-modal')
    .querySelector('button.modal-close')
    .addEventListener('click', onCloseClick)

  window.document.getElementById('canvas')
    .addEventListener('mousedown', onCanvasClick)

  window.document.getElementById('canvas')
    .addEventListener('touchstart', onCanvasClick)
}
