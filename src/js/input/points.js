import * as _ from 'lodash'

import tileStore from '../stores/tileStore'
import playerStore from '../stores/playerStore'
import settingStore from '../stores/settingStore'
import net from '../net'


export const addPointAtLocation = _.throttle(() => {
  playerStore.local.line.push({
    position: playerStore.local.position,
    size: playerStore.local.size
  })
}, 5)

export function commitPoints() {
  let tempId = undefined
  if (!settingStore.debug.disableLocalPaints)
    tempId = tileStore.addLocalLine({
      line: playerStore.local.line,
      color: playerStore.local.color
    })

  net.sendPaint(playerStore.local.line, playerStore.local.color, tempId)
}
