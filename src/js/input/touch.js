import playerStore from '../stores/playerStore'
import viewStore from '../stores/viewStore'
import * as consts from '../consts'

import { addPointAtLocation, commitPoints } from './points'


let lastDistance = null
let lastOrigin = null
let zoomDeadzone = null
let lastTouch = null


function onTouchStart(e) {
  playerStore.setLocalPosition([
    ((e.touches.item(0).clientX - (e.target.width / 2)) / viewStore.scale) +
      viewStore.pan[0],
    ((e.touches.item(0).clientY - (e.target.height / 2)) / viewStore.scale) +
      viewStore.pan[1]
  ]);


  if (viewStore.tool === consts.TOOL_PEN) {
    playerStore.local.painting = true
    playerStore.local.line = [{
      position: playerStore.local.position,
      size: playerStore.local.size
    }]
  }

  lastDistance = null
  lastOrigin = null
  zoomDeadzone = null
  lastTouch = null
}

function onTouchEnd() {
  if (viewStore.tool === consts.TOOL_PEN) {
    commitPoints()
    playerStore.local.painting = false
    playerStore.local.line = []
  }
}

function onTouchMove(e) {
  if (e.touches.length > 1) {
    let mainTouch = null
    let altTouch = null

    if (e.touches.item(0).clientX > e.touches.item(1).clientX) {
      mainTouch = e.touches.item(0)
      altTouch = e.touches.item(1)
    } else {
      mainTouch = e.touches.item(1)
      altTouch = e.touches.item(0)
    }

    const dist = Math.sqrt(
      Math.pow((mainTouch.clientX - altTouch.clientX), 2) +
      Math.pow((mainTouch.clientY - altTouch.clientY), 2)
    )

    if (lastDistance != null) {
      const distDelta = lastDistance - dist
      zoomDeadzone += Math.abs(distDelta)

      if (zoomDeadzone >= 50)
        viewStore.setScale(viewStore.scale - distDelta * 0.01)
    }

    lastDistance = dist

    const origin = [
      (mainTouch.clientX + altTouch.clientX) / 2,
      (mainTouch.clientY + altTouch.clientY) / 2
    ]

    if (lastOrigin != null) {
      const originDelta = [
        origin[0] - lastOrigin[0],
        origin[1] - lastOrigin[1]
      ]

      viewStore.setPan([
        viewStore.pan[0] - (originDelta[0] * 2 / viewStore.scale),
        viewStore.pan[1] - (originDelta[1] * 2 / viewStore.scale)
      ])
    }

    lastOrigin = origin
  } else {
    if (viewStore.tool === consts.TOOL_PEN) {
      addPointAtLocation()
      if (playerStore.local.line.length >= 256) {
        commitPoints()
        playerStore.local.line = playerStore.local.line.slice(-1)
      }
    } else if (viewStore.tool === consts.TOOL_PAN) {
      if (lastTouch !== null) {
        const touchDelta = [
          lastTouch[0] - e.touches.item(0).clientX,
          lastTouch[1] - e.touches.item(0).clientY
        ]

        viewStore.setPan([
          viewStore.pan[0] + (touchDelta[0] * 2 / viewStore.scale),
          viewStore.pan[1] + (touchDelta[1] * 2 / viewStore.scale)
        ])
      }

      lastTouch = [
        e.touches.item(0).clientX,
        e.touches.item(0).clientY
      ]
    } else if (viewStore.tool === consts.TOOL_ZOOM) {
      if (lastTouch !== null) {
        const touchDelta = [
          lastTouch[0] - e.touches.item(0).clientX,
          lastTouch[1] - e.touches.item(0).clientY
        ]

        viewStore.setScale(viewStore.scale + touchDelta[1] * 0.01)
      }

      lastTouch = [
        e.touches.item(0).clientX,
        e.touches.item(0).clientY
      ]
    }
  }

  playerStore.setLocalPosition([
    ((e.touches.item(0).clientX - (e.target.width / 2)) / viewStore.scale) +
      viewStore.pan[0],
    ((e.touches.item(0).clientY - (e.target.height / 2)) / viewStore.scale) +
      viewStore.pan[1]
  ]);

  e.preventDefault()
}


export function attach(canvasDom) {
  canvasDom.addEventListener('touchstart', onTouchStart)
  canvasDom.addEventListener('touchend', onTouchEnd)
  canvasDom.addEventListener('touchmove', onTouchMove)
}
