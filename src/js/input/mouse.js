import * as _ from 'lodash'

import playerStore from '../stores/playerStore'
import viewStore from '../stores/viewStore'
import * as consts from '../consts'

import { addPointAtLocation, commitPoints } from './points'


const _LEFT_MOUSE = 1
const _MIDDLE_MOUSE = 4


function _isPainting(e) {
  return (viewStore.tool === consts.TOOL_PEN) && (playerStore.local.painting ||
    (e.buttons & _LEFT_MOUSE) === _LEFT_MOUSE)
}

function _isPanning(e) {
  return (viewStore.tool === consts.TOOL_PAN &&
    (e.buttons & _LEFT_MOUSE) === _LEFT_MOUSE) || (
      (e.shiftKey && (e.buttons & _LEFT_MOUSE) === _LEFT_MOUSE) ||
      (e.buttons & _MIDDLE_MOUSE) === _MIDDLE_MOUSE)
}

function _isZooming(e) {
  return (viewStore.tool === consts.TOOL_ZOOM &&
    (e.buttons & _LEFT_MOUSE) === _LEFT_MOUSE)
}


function onMouseMove(e) {
  if (_isPanning(e)) {
    viewStore.setPan([
      viewStore.pan[0] - e.movementX / viewStore.scale,
      viewStore.pan[1] - e.movementY / viewStore.scale
    ])
  } else if (_isPainting(e)) {
    addPointAtLocation()
    if (playerStore.local.line.length >= 256) {
      commitPoints()
      playerStore.local.line = playerStore.local.line.slice(-1)
    }
  } else if (_isZooming(e)) {
    viewStore.setScale(viewStore.scale + (e.movementY * 0.01))
  }

  playerStore.setLocalPosition([
    ((e.clientX - (e.target.width / 2)) / viewStore.scale) +
      viewStore.pan[0],
    ((e.clientY - (e.target.height / 2)) / viewStore.scale) +
      viewStore.pan[1]
  ]);
}

function onMouseDown(e) {
  if (_isPainting(e) && !_isPanning(e) && !_isZooming(e)) {
    playerStore.local.painting = true
    playerStore.local.line = [{
      position: playerStore.local.position,
      size: playerStore.local.size
    }]
  }

  e.preventDefault()
}

function onMouseUp(e) {
  if (_isPainting(e) && !_isPanning(e) && !_isZooming(e)) {
    commitPoints()

    playerStore.local.painting = false
    playerStore.local.line = []
  }

  e.preventDefault()
}

function onWheel(e) {
  // Use both e.deltaX and e.deltaY so people with weird mice can scroll in
  // any direction. This also fixes an issue in Chrome where shift + wheel
  // lets you scroll horizontally, BUT it also means that e.deltaX is filled
  // instead of e.deltaY
  const scrollDir = Math.sign(e.deltaX + e.deltaY)

  if (e.shiftKey || (
    viewStore.tool === consts.TOOL_PAN ||
    viewStore.tool === consts.TOOL_ZOOM
  )) {
    viewStore.setScale(viewStore.scale - (scrollDir * 0.05))
    playerStore.setLocalPosition([
      ((e.clientX - (e.target.width / 2)) / viewStore.scale) +
        viewStore.pan[0],
      ((e.clientY - (e.target.height / 2)) / viewStore.scale) +
        viewStore.pan[1]
    ]);

    e.preventDefault()
  } else {
    playerStore.local.size =
      _.clamp(playerStore.local.size - (scrollDir * 1), 4, 32)
  }
}


export function attach(canvasDom) {
  canvasDom.addEventListener('mousemove', onMouseMove)
  canvasDom.addEventListener('mousedown', onMouseDown)
  canvasDom.addEventListener('mouseup', onMouseUp)
  canvasDom.addEventListener('wheel', onWheel)
}
