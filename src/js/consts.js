export const TOOL_PEN = 'pen'
export const TOOL_PAN = 'pan'
export const TOOL_ZOOM = 'zoom'

export const CHAT_MAX_LINES = 128
