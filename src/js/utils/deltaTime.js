export default new class DeltaTime {
  constructor() {
    this.lastTime = 0

    this.time = 0
    this.timeSecs = 0
    this.delta = 0
    this.deltaSecs
  }

  tick() {
    this.time = window.performance.now()
    this.delta = this.time - this.lastTime

    this.timeSecs = this.time / 1000
    this.deltaSecs = this.delta / 1000

    this.lastTime = this.time
  }
}
