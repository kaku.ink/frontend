const path = require('path')
const autoprefixer = require('autoprefixer')
const webpack = require('webpack')

const configDefaults = {
  context: path.resolve(__dirname, './src/js'),
  entry: ['babel-polyfill', path.resolve(__dirname, './src/js/index.js')],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './bin')
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: 'es2015'
        }
      },
      {
        test: /\.scss$/,
        loader: 'style!css!postcss!sass'
      }
    ]
  },

  plugins: [],
  postcss: [autoprefixer]
};

const configDefaultPlugins = [
  new webpack.EnvironmentPlugin([
    'KAKU_FRONTEND_OVERWORLD_URL',
    'KAKU_FRONTEND_TILE_BASE'
  ])
]


const configDev = {
  devtool: 'inline-source-map'
}


const configProduction = {

}

const configProductionPlugins = [
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.UglifyJsPlugin({
    sourceMap: false,
    compress: {
      warnings: false
    }
  })
]

if (process.env.NODE_ENV === 'production') {
  module.exports = Object.assign({}, configDefaults, configProduction)
  Array.prototype.push.apply(module.exports.plugins, configDefaultPlugins)
  Array.prototype.push.apply(module.exports.plugins, configProductionPlugins)
} else {
  module.exports = Object.assign({}, configDefaults, configDev)
  Array.prototype.push.apply(module.exports.plugins, configDefaultPlugins)
}
